package metodos.ageis;

public class Disciplina {
	
	private String nomeDisciplina;
	private String codDisciplina;
	
	public Disciplina(String nomeDisciplina, String codDisciplina) {
		this.nomeDisciplina = nomeDisciplina;	
		this.codDisciplina = codDisciplina;
	}

	public String getNomeDisciplina() {
		return nomeDisciplina;
	}

	public void setNomeDisciplina(String nomeDisciplina) {
		this.nomeDisciplina = nomeDisciplina;
	}

	public String getCodDisciplina() {
		return codDisciplina;
	}

	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}	
}

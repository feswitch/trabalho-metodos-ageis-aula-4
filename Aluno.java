package metodos.ageis;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Aluno extends Candidato {
	
	private Integer numMatricula;
	private String cpf;

	public Aluno(String nome, String formacaoAcademica, Integer rg, String telefone, String endereco, String email,
			Date dataNascimento) {
		super(nome, formacaoAcademica, rg, telefone, endereco, email, dataNascimento);
	}

	public Integer getNumMatricula() {
		return numMatricula;
	}

	public void setNumMatricula(Integer numMatricula) {
		this.numMatricula = numMatricula;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
}
